import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Trans.Error
import           Control.Monad.Trans.Except
import           Control.Monad.Writer
import qualified Data.Map                   as Map
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
  deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
  deriving (Show )

exampleExp = Lit 12  `Plus`  (App (Abs "x" (Var "y"))
                                (Lit 4  `Plus`  Lit 2))

-- type Env = Map.Map Name Value

-- eval0 :: Env -> Exp -> Value

-- eval0 env (Lit i) = IntVal i
-- eval0 env (Var n) = fromJust (Map.lookup n env)
-- eval0 env (Plus e1 e2) = let (IntVal i1) = eval0 env e1
--                              (IntVal i2) = eval0 env e2
--                          in IntVal (i1+i2)
-- eval0 env (Abs n e) = FunVal env n e
-- eval0 env (App e1 e2) = let  val1 = eval0 env e1
--                              val2 = eval0 env e2
--                         in case val1 of
--                             FunVal env n body ->
--                               eval0 (Map.insert n val2 env) body

-- ans = eval0 Map.empty exampleExp

------------------------------------------------

type Eval1 a = Identity a

type Env = Map.Map Name (Eval1 a)

eval1 :: Env -> Exp -> Eval1 a

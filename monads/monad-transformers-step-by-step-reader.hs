{-

newtype Reader r a = Reader {runReader :: r -> a}
compare with
newtype State s a = State {runState :: s -> (a,s)}

newtype ReaderT r m a = ReaderT {runReaderT :: r -> m a}

Monad m => Monad (ReaderT r m)
   where return i = (lift . return) i
         m1 >>= k = ReaderT \r -> do
                            i <- runReaderT m1 r
                            runreaderT (k i) r

(ReaderT r) is an element of Monadtrans  class. If t is an instance of
the Monadtrans, then one of the  functions t should support is lift ::
m a -> t m a.



Monad m => MonadReader r (ReaderT r m)
   MonadReader r m means m is a monad which can also do some operations
   related to an "environment" of the type r

   ask :: ReaderT r m r -- retrieves the environment packed in a monad

   ask = ReaderT return -- return of the inner monad

   local:: (r -> r) -> ReaderT r m a -> ReaderT r m a
   local f rm1 = rm2
    where rm2 = ReaderT  (\r -> runReaderT rm1 (f r))
              = ReaderT $ runReaderT rm1 . f
-}

import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.Trans.Except
import qualified Data.Map                   as M
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
   deriving (Show )

type Env = M.Map Name (ExceptT String Identity Value)


----------------------------------------------------------

type Eval3 a = ReaderT Env (ExceptT String Identity) a

runEval3 :: Env -> Eval3 a -> Either String a

runEval3 env ev3 = runIdentity (runExceptT (runReaderT ev3 env ))

eval3 :: Exp -> Eval3 Value

eval3 (Lit i) = return $ IntVal i

eval3 (Var v) = do env <- ask
                   case M.lookup v env of
                     Nothing -> (lift . throwE) ("unbound variable: " ++ v)
                     Just val -> ReaderT (\r -> val)  

eval3 (Plus e1 e2 ) = do e1' <- eval3 e1
                         e2' <- eval3 e2
                         case (e1', e2') of
                            (IntVal i1 , IntVal i2 ) ->
                                return $ IntVal (i1 + i2 )
                            _  -> (lift . throwE) ("type error in addition")

eval3 (Abs n e)     = do env <- ask
                         return $ FunVal env n e

eval3 (App e1 e2 ) = do val1 <- eval3 e1
                        val2 <- eval3 e2
                        case val1 of
                           FunVal env' n body ->
                              local (const (M.insert n (return val2) env' ))
                                    (eval3 body)
                           _ -> (lift . throwE) "type error in application"


exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))

exampleExp5 = Lit 12  `Plus`  (App (Abs "x" (Var "x"))
                             (Lit 4  `Plus`  Lit 2))

ans1 = runEval3 M.empty (eval3 exampleExp1)
ans2 = runEval3 M.empty (eval3 exampleExp2)
ans3 = runEval3 M.empty (eval3 exampleExp3)
ans4 = runEval3 M.empty (eval3 exampleExp4)
ans5 = runEval3 M.empty (eval3 exampleExp5)

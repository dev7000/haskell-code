import           Control.Monad.Identity

import qualified Data.Map                   as M
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
   deriving (Show )

-- The  goal of  using monad  transformers is  to have  control over
-- aspects  of computations,  such  as  state, errors,  environments
-- etc.  It is  a  bit  tedious to  reformulate  an already  written
-- program in monadic style, but once that is done, it is relatively
-- easy to add, remove or change the monads involved.

-- newtype Identity a = Identity { runIdentity :: a }
-- instance Monad Identity where
--  return a = Identity a
--  m >>= k  = k (runIdentity m)

type Env = M.Map Name (Identity Value) -- May have to change later

eval1 :: Env -> Exp -> Identity Value

eval1 env (Lit i ) = return $ IntVal i
eval1 env (Var n) =  fromJust (M.lookup n env)           --source of error
eval1 env (Plus e1 e2 ) = do val1 <- eval1 env e1        --source of error
                             val2 <- eval1 env e2
                             case (val1, val2) of
                               (IntVal i1, IntVal i2) -> return $ IntVal (i1 + i2 )


eval1 env (Abs n e) = return $ FunVal env n e
eval1 env (App e1 e2 ) = do val1 <- eval1 env e1
                            val2 <- eval1 env e2
                            case val1 of
                              FunVal env n body ->       --source of error
                                eval1 (M.insert n (return val2) env )
                                body


exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))

ans0 = runIdentity $ eval1  M.empty (Var "y")
ans1 = runIdentity $ eval1  M.empty exampleExp1
ans2 = runIdentity $ eval1  M.empty exampleExp2
ans3 = runIdentity $ eval1  M.empty exampleExp3
ans4 = runIdentity $ eval1  M.empty exampleExp4

---------------------------------------------------



import           Control.Monad.Identity
import           Control.Monad.Trans.Except
  
import qualified Data.Map                   as M
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
   deriving (Show )

-- From monads to monad transformers

-- http://hackage.haskell.org/package/mtl-2.2.2/docs/Control-Monad-Error.html


-- newtype ErrorT e (m :: * -> *) a = ErrorT {runErrorT :: m (Either e a)}
   

--  o Error  a is a class  whose instances can throw  an exception.
--  o  ErrorT is  a monad  transformer. It  transform the  type (m  a) to
--  ErrorT e  (m a). A value  of the type ErrorT  e (m a) is  either a
--  m-monadic (Left  e)), where e  is an  error value, or  a m-monadic
--  (Right a). In either case, the value of the type m (Either e a) is
--  packed within a ErrorT data constructor.

--  o A transformer of (m a) wraps the contained value a. 

-- instance (Monad m, Error e) => Monad (ErrorT e m) where
--   return a = ErrorT $ return (Right a)                              -- return is the return of m

--   In the following, m :: ErrorT e m a, k :: a -> ErrorT e m a
--   m >>= k = ErrorT $
--    do
--     a <- runErrorT m
--     case a of
--       Left l -> return (Left l)
--       Right r -> runErrorT (k r)
--   fail msg = ErrorT $ return (Left (strMsg msg))


-- instance (Monad m, Error e) => MonadError e (ErrorT e m) where
--   throwError l = ErrorT $ return (Left l)
--   m  `catchError`  h = ErrorT $ do
--     a <- runErrorT m
--   case a of
--     Left l -> runErrorT (h l)
--     Right r -> return (Right r)


type Env = M.Map Name (Eval2 Value)

type Eval2 a = ExceptT String Identity a

runEval2 :: Eval2 a -> Either String a

runEval2 ev = runIdentity (runExceptT ev )

eval2 :: Env -> Exp -> Eval2 Value

eval2 env (Lit i ) = return $ IntVal i     -- return of ExceptT String Identity a, which is a monad

eval2 env (Var n) = case (M.lookup n env) of
                      Nothing -> throwE $ "Could not find the variable " ++ n -- Exception signalled
                      Just res  -> res
eval2 env (Plus e1 e2 ) = do  e2' <- eval2 env e2  -- Handled by fail of monad
                              e1' <- eval2 env e1
                              case (e1', e2') of
                               (IntVal i1, IntVal i2) -> return $ IntVal (i1 + i2 )
                               _ -> throwE "One of the operands of Plus was not an Integer" -- Exception signalled

eval2 env (Abs n e) = return $ FunVal env n e
eval2 env (App e1 e2 ) = do  val1 <- eval2 env e1
                             val2 <- eval2 env e2
                             case val1 of
                               FunVal env n body ->
                                 eval2 (M.insert n (return val2) env ) body
                               _ -> throwE "type error in application" -- Exception signalled.

exampleExp0 = (Lit 4  `Plus`  Lit 2)

exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))

ans0 = runEval2 $ eval2 M.empty exampleExp0
ans1 = runEval2 $ eval2 M.empty exampleExp1
ans2 = runEval2 $ eval2 M.empty exampleExp2
ans3 = runEval2 $ eval2 M.empty exampleExp3
ans4 = runEval2 $ eval2 M.empty exampleExp4



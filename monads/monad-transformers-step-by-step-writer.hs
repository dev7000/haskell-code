-- newtype WriterT w (m :: * -> *) a = WriterT {runWriterT :: m(a,w)}

-- (Monoid w, Monad m) => Monad (WriterT w m)
-- (Monoid w, MonadError e m) => MonadError e (WriterT w m)
-- (Monoid w, MonadReader r m) => MonadReader r (WriterT w m)
-- (Monoid w, MonadState s m) => MonadState s (WriterT w m)
-- (Monoid w, Monad m) => MonadWriter w (WriterT w m)


-- class (Monoid w, Monad m) => MonadWriter w m | m -> w

-- (writer | tell), listen, pass

-- Methods

-- writer :: (a, w) -> m a

-- writer (a,w) embeds a simple writer action.

-- tell :: w -> m ()

-- tell w is an action that produces the output w.

-- listen :: m a -> m (a, w)

-- listen m is an action that executes the action m and adds its output to the value of the computation.

-- pass :: m (a, w -> w) -> m a

-- pass m is an action that executes the action m, which returns a value and a function, and returns the value, applying the function to the output.

---------------------------------------------------------------
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Trans.Error
import           Control.Monad.Trans.Except
import           Control.Monad.Writer
import qualified Data.Map                   as Map
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
--   deriving (Show)

instance Show Value where
 show (IntVal i) = "IntVal " ++ show i
 show (FunVal e n exp) = "\\" ++ show n ++ "->" ++  show exp

--tick :: (Num s, MonadState s m) => m ()
tick = do st <- get
          put (st + 1)

type Env = Map.Map Name (Eval2 Value)

type Eval2 a = ErrorT String
                      (WriterT [String] (StateT Integer Identity)) a

type Eval5 a = ReaderT Env (ErrorT String
                       (WriterT [String ] (StateT Integer Identity))) a

runEval5 :: Env ->  Integer -> Eval5 a ->  ((Either String a, [String]), Integer)
runEval5 env st ev = runIdentity (runStateT (runWriterT (runErrorT
                                 (runReaderT ev env ))) st)

eval5 :: Exp -> Eval5 Value
eval5 (Lit i)= do tick
                  return $ IntVal i
eval5 (Var n)= do tick
                  tell [n]
                  env <- ask
                  case Map.lookup n env of
                    Nothing -> (lift . throwError) ("unbound variable: " ++ n)
                    Just val -> ReaderT (\r -> val)
eval5 (Plus e1 e2 ) = do tick
                         e1 <- eval5 e1
                         e2 <- eval5 e2
                         case (e1 , e2 ) of
                           (IntVal i1 , IntVal i2 ) ->
                             return $ IntVal (i1 + i2 )
                           _ -> (lift . throwError) "type error in addition"
eval5 (Abs n e) = do tick
                     env <- ask
                     return $ FunVal env n e
eval5 (App e1 e2 ) = do tick
                        val1 <- eval5 e1
                        val2 <- eval5 e2
                        case val1 of
                          FunVal env n body ->
                              local (const (Map.insert n (return val2) env )) (eval5 body)
                          _ -> (lift . throwError) "type error in application"

ans1 = runEval5 Map.empty 0 (eval5 exampleExp1)
ans2 = runEval5 Map.empty 0 (eval5 exampleExp2)
ans3 = runEval5 Map.empty 0 (eval5 exampleExp3)
ans4 = runEval5 Map.empty 0 (eval5 exampleExp4)

exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))

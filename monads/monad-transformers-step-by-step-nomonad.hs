import qualified Data.Map                   as M
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show)

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
   deriving (Show )


type Env = M.Map Name Value

----------non-monadic interpretation ----------

eval0 :: Env -> Exp -> Value
eval0 env (Lit i) = IntVal i
eval0 env (Var n) = fromJust (M.lookup n env )
eval0 env (Plus e1 e2 ) =
  let IntVal i1 = eval0 env e1                   --source of error
      IntVal i2 = eval0 env e2
  in IntVal (i1 + i2 )
eval0 env (Abs n e) = FunVal env n e
eval0 env (App e1 e2 ) =
  let val1 = eval0 env e1
      val2 = eval0 env e2
  in case val1 of                                --source of error
       FunVal env' n body -> eval0 (M.insert n val2 env') body

--Examples
exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))


ans1 = eval0  M.empty exampleExp1
ans2 = eval0  M.empty exampleExp2
ans3 = eval0  M.empty exampleExp3
ans4 = eval0  M.empty exampleExp4

{-
newtype StateT s m a = StateT { runStateT :: s -> m (a,s) }

return a = StateT $ \ s -> return (a, s)
    m >>= k  = StateT $
               \ s -> do
                        ~(a, s') <- runStateT m s
                        runStateT (k a) s'
    fail str = StateT $ \ _ -> fail str

class Monad m => MonadState s m | m -> s where

Minimal definition is either both of get and put or just state

Methods

get :: m s
put :: s -> m ()


-}

-----------------------------------------------------
import           Control.Monad.State
import           Control.Monad.Trans.State.Lazy
import           Control.Monad.Trans.Except
import           Control.Monad.Writer
import qualified Data.Map                   as M
import           Data.Maybe
--------------------------------------------

type Name = String -- variable names
data Exp = Lit Integer  -- expressions
         | Var Name
         | Plus Exp Exp
         | Abs Name Exp
         | App Exp Exp
   deriving (Show )

data Value = IntVal Integer -- values
           | FunVal Env Name Exp
--   deriving (Show)

instance Show Value where
 show (IntVal i) = "IntVal " ++ show i
 show (FunVal e n exp) = "\\" ++ show n ++ "->" ++  show exp

--tick :: (Num s, MonadState s m) => m ()

tick = do st <- get
          put (st + 1)

type Env = M.Map Name (Eval2 Value)

type Eval2 a = ErrorT String (StateT Integer Identity) a



runEval4 env st ev = runIdentity (runStateT (runErrorT (runReaderT ev env )) st)



eval4 :: Exp -> Eval4 Value

eval4 (Lit i ) = do tick
                    return $ IntVal i
eval4 (Var n)  = do tick
                    env <- ask
                    case M.lookup n env of
                      Nothing -> (lift.throwError) ("unbound variable: " ++ n)
                      Just val -> ReaderT (\r -> val)
eval4 (Plus e1 e2 ) = do tick
                         e1 <- eval4 e1
                         e2 <- eval4 e2
                         case (e1 , e2 ) of
                           (IntVal i1 , IntVal i2 ) ->
                             return $ IntVal (i1 + i2 )
                           _ -> (lift.throwError) "type error in addition"
eval4 (Abs n e) = do tick
                     env <- ask
                     return $ FunVal env n e
eval4 (App e1 e2 ) = do tick
                        val1 <- eval4 e1
                        val2 <- eval4 e2
                        case val1 of
                          FunVal env n body ->
                            local (const (M.insert n (return val2) env ))
                            (eval4 body)
                          _ -> (lift.throwError) "type error in application"

ans1 = runEval4 M.empty 0 (eval4 exampleExp1)
ans2 = runEval4 M.empty 0 (eval4 exampleExp2)
ans3 = runEval4 M.empty 0 (eval4 exampleExp3)
ans4 = runEval4 M.empty 0 (eval4 exampleExp4)

exampleExp1 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "y"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp2 = (Abs "x" (Var "x"))  `Plus`  (App (Abs "x" (Var "x"))
                                           (Lit 4  `Plus`  Lit 2))
exampleExp3 = (Lit 16  `Plus`  (App (Abs "x" (Var "x"))
                                    (Lit 4  `Plus`  Lit 2)))
exampleExp4 = (Abs "x" (Var "y"))


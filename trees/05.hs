data Gtree a = Gnode a [Gtree a] deriving (Show)
data Btree a = Bnode (Btree a) (Btree a) | Leaf a deriving (Show)

g_to_b :: Gtree a -> Btree a
b_to_g :: Btree a -> Gtree a
 

eg_g = Gnode 'f' [Gnode 'g' [Gnode 'x' [], Gnode 'l' []],
       Gnode 'h' [Gnode 'l' []], Gnode '5' []]

eg_b = Bnode (Bnode (Bnode (Leaf 'f')
       (Bnode (Bnode (Leaf 'g') (Leaf 'x'))
       (Leaf 'l')))
       (Bnode (Leaf 'h') (Leaf 'l')))
       (Leaf '5')

g_to_b (Gnode v []) = Leaf v 
g_to_b (Gnode v l) = Bnode (g_to_b (Gnode v (init l))) (g_to_b (last l))


b_to_g (Leaf v) = Gnode v []
b_to_g (Bnode x y) = Gnode a (l ++ [b_to_g y])
  where (Gnode a l) = b_to_g x
  
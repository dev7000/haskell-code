data Htree a = Null | Fork a (Htree a) (Htree a) deriving (Show)

levels :: [a] -> [[a]]

levels list = reverse $ helper list [] 1
  where helper [] r len = r
        helper l r len = let (split, left) = splitAt len (l)
                         in helper left (split:r) (2*len)

mktree :: [[a]] -> [Htree a]

mktree = foldr addLayer [Null]

addLayer upper lower = addLayer1 upper (complete lower ((length upper)*2))

complete l len = l ++ (replicate (len - (length l)) Null)

addLayer1 upper lower = [ Fork v x y | (v,x,y)<-zip3 upper (evenodds lower 0) (evenodds lower 1)]

evenodds l place = [ x|(x,y) <- (zip l [0..]) ,rem y 2 == place]
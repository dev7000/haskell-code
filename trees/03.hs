data RadixTree = Leaf Color | Node Color RadixTree RadixTree deriving (Show)
data Color = Black | White deriving (Show)

insert :: [Int] -> RadixTree -> RadixTree


insert string tree | all (\t -> (t == 0) || (t == 1)) string = insert1 string tree
                   | otherwise = error "give proper input"

insert1 [] (Leaf White) = error "already present"  
insert1 [] (Leaf Black) = Leaf White
insert1 [] (Node White x y) = error "already present"
insert1 [] (Node Black x y) = Node White x y

insert1 (x:xs) (Leaf v) | x == 1 = (Node v (Leaf Black) (insert xs (Leaf Black)))
                       | otherwise = (Node v (insert xs (Leaf Black)) (Leaf Black))

insert1 (x:xs) (Node v p q) | x == 1 = (Node v p (insert xs q))
                           | otherwise = (Node v (insert xs p) q)                      

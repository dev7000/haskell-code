data Tree a = Node (Tree a) a (Tree a) | Null deriving (Show) 

dia Null = 0
dia (Node x p y) = maximum [(dia x), (dia y), (height x) + (height y) + 1] 
  where height Null = 0
        height (Node x p y) = 1 + (max (height x) (height y))


exampletree = Node (Node (Node Null () Null) () (Node (Node Null () Null) 
              () (Node Null () Null))) () (Node Null () (Node Null () (Node (Node
              (Node Null () Null) () (Node Null () Null)) () (Node Null () Null))))



data Btree a = BNode (Btree a) (Btree a) | Leaf a deriving (Show)


decode :: [Int] -> Btree Char -> [Char]

decode code tree = reverse $ decodehelper code [] tree

longtraverse (Leaf x) l = (x,l)
longtraverse (BNode _ _) [] = error "lost connection"
longtraverse (BNode p q) (x:xs) | x == 0 = longtraverse p xs
                                | x == 1 = longtraverse q xs
                                | otherwise = error "i don't know"

decodehelper [] r tree = r
decodehelper l r tree = decodehelper left (dest:r) tree 
  where (dest, left) = longtraverse tree l

--------------------------------------------------------------------------------------------------

encode:: [Char] -> Btree Char -> [Int]

searchletter l (Leaf x) = ([],x==l)
searchletter l (BNode p q) | x && y = error "double standards found"
                           | x = (0:pathx,True)
                           | y = (1:pathy,True)
                           | otherwise = ([],False)

  where (pathx,x) = searchletter l p 
        (pathy,y) = searchletter l q

encode chars tree | all (== True) res = concat codes
                  | otherwise = error "all letters not found"
  where (codes,res) = unzip [searchletter c tree | c<-chars]


ht = BNode (Leaf 'a') (BNode (BNode (Leaf 'b') (BNode (Leaf 'c') (Leaf 'd'))) 
     (BNode (BNode (Leaf 'e') (Leaf 'f')) (BNode (Leaf 'g') (Leaf 'h'))))


-- decode code tree = reverse.fst $ foldl helper ([],tree) code


--   where helper (res,t) path = let nextnode = (traverse t path, checkLeaf (fst nextnode))
--                               in if (snd nextnode)
--                               then ((getVal (fst nextnode)):res,tree) 
--                               else (res,(fst nextnode))
                                  
--         traverse (Leaf x) _ = error "ran over leaf"
--         traverse (BNode x y) p | p == 0 = x
--                                | p == 1 = y
--                                | otherwise = error "iam not sure" 
--         checkLeaf (Leaf _) = True
--         checkLeaf (BNode _ _) = False    
--         getVal (Leaf x) = x
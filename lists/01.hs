import Test.QuickCheck
import Control.Monad

errno x = helper x 0 0
  where helper [] c r = c+r
        helper (x:xs) c r | x == '(' = helper xs (c+1) r
                          | x /= ')' = error "unknown char found"
                          | c == 0 = helper xs c (r+1)
                          | otherwise = helper xs (c-1) r




data Brak = Brak [Char] deriving (Show)
instance Arbitrary Brak where
  arbitrary = liftM Brak (listOf (elements "()"))


prop_errno :: Brak -> Property
prop_errno (Brak l) = collect l $ errno l == errno (invert l)


invert l = foldl add [] l
add r x | x == '(' = ')':r
        | x == ')' = '(':r
        | otherwise = error "unknown char found"

import Test.QuickCheck

pascal = [1] : [(helper x) | x <- pascal]
  where helper x = [1] ++ (zipWith (+) x (tail x)) ++ [1]


prop_pascal :: Int -> Property
prop_pascal x = collect x $ x >=0 ==> pascal !! x == reverse (pascal !! x)



type Node = Int
type Graph = [(Node,Node)]
type Path = [Node]

complete :: Graph -> Graph

complete = foldr add []
  where add x l | all (\t -> x /= t) l = [x] ++ (nflip x) ++ l
                | otherwise = l

nflip (p,q) | p /= q = [(q,p)]
            | otherwise = []

makepath :: Node -> Graph -> [Path]

makepath n = extend [[n]] []
extend [] l g = l
extend p l g = extend (concat $ map (\t -> [ y:t | (x,y) <- g, x == (head t), (all ((/=) y) t)]) p) (p ++ l) g

is_hc :: Path -> Graph -> Bool

is_hc p g = length p == length (allnodes g) 
  
lastedge p g = any (\t -> t == e) g
  where e = ((head p), (last p))

allnodes g = foldl (\l (x,y) -> add x (add y l)) [] g
  where add x l | all (\t -> t /= x) l = x:l
                | otherwise = l

hcycles :: Graph -> [Path]

hcycles g = clean (concat (map (\x -> (filter (\t -> (length t == gl) && (lastedge t cg)) (makepath x cg))) an)) []
  where cg = complete g
        an = allnodes g
        gl = length an

clean [] r = r
clean (x:xs) r = clean (foldl (\l t -> filter ((/=) t) l) xs ((makeperm x) ++ (makeperm (reverse x)))) (x:r) 


makeperm x = zipWith (++) (tails x) (inits x)

inits [] = [[]]
inits (x:xs) = [] : (map (x:) (inits xs))

tails [] = [[]]
tails (x:xs) = (x:head l):l
  where l = tails xs
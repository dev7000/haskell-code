import Test.QuickCheck

summands x | x < 0 = error "give correct inputs"
           | x == 0 = [[]]
           | otherwise = concat $ map (\t -> map (t:) (summands (x-t))) [1..x]


prop_summands :: Int -> Property
prop_summands x = collect x $ x >=0 ==> all (\t->(sum t) == x) (take 100 (summands x))

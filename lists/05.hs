cprod [] = [[]]
cprod (x:xs) = concat $ map (\x -> map (x:) y) x
  where y = cprod xs 
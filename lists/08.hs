ssm [] = [[]]
ssm (x:y) = map (x:) sols 
  where f _ [] = True
        f x (y:z) = x < y
        temp = concat $ map ssm $ filter (f x) $ scanr (:) [] y
        maxx = maximum (map length temp)
        sols = filter (\x -> ((length x) == maxx)) temp
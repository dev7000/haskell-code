import Test.QuickCheck

powers x = 1 : [x*y | y<- (powers x)]


prop_powers :: Int -> Bool
prop_powers x = (powers x)!!1 == x  

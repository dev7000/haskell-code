import Test.QuickCheck
import Control.Monad


fewest_moves [] = 0
fewest_moves (x:xs) | x == 0 = 1 +  (min (helper (x:xs) 1) (helper (x:xs) 4))
                    | x == 1 = 1 +  (min (helper (x:xs) 1) (helper (x:xs) 2))
  where helper l d | length l < d = (maxBound :: Int)
                   | otherwise = fewest_moves (drop d l)


data List01 = List01 [Int] deriving (Show)
instance Arbitrary List01 where
  arbitrary = liftM List01 (suchThat (listOf (elements [0,1])) (\t -> length t < 20))


prop_fmoves :: List01 -> Property
prop_fmoves (List01 x) = collect x $ fewest_moves x <= (length x)



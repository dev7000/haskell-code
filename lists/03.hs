import Test.QuickCheck

lcs [] _ = []
lcs _ [] = []
lcs (x:xs) (y:ys) = helper
  where helper | x == y = x : (lcs xs ys)
               | otherwise = choose (lcs (x:xs) ys) (lcs xs (y:ys))
        choose x y | length x > (length y) = x
                   | otherwise = y

prop_lcs :: [Int] -> [Int] -> Property

prop_lcs x y = collect x $ length (lcs x y) == (length (lcs y x))

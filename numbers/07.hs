data Set a = Set (a -> Bool)

insert (Set f) element = Set (\x -> f x || x == element) 
member (Set f) element = f element
union (Set f) (Set g) = Set (\x -> f x || g x)
intersection (Set f) (Set g) = Set (\x -> f x && g x)
difference (Set f) (Set g) = Set (\x -> f x && not (g x))

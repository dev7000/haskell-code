--is_palindromic n base | base == 1 = True
--                      | otherwise = ((==).reverse) seq seq
--  where seq = transform n 
--        transform x | x == 0 = []
--                    | otherwise = ((rem x base):(transform (quot x base)))  

--count_palindromes = helper [0..1000000] 0
--  where helper [] c = c
--        helper (x:xs) c | (is_palindromic x 10) && (is_palindromic x 8) = helper xs (c+1)
--                        | otherwise = helper xs c



is_palindromic n base | base == 1 = True
                      | otherwise = (numreverse 0 seq) == seq
  where seq = transform n 
        transform x | x == 0 = 0
                    | otherwise = ((rem x base) + 10* (transform (quot x base)))
        numreverse r x | x == 0 = r
                       | otherwise = numreverse (rem x 10 + 10*r) (quot x 10)

count_palindromes = helper 1000000 0
  where helper (-1) c = c
        helper x c | (is_palindromic x 10) && (is_palindromic x 8) = helper (x-1) (c+1)
                   | otherwise = helper (x-1) c

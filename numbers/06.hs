filtered_accumulate filter reduce a b identity = helper a b identity
  where helper x y r | x > y = r
                     | filter x = helper (x+1) y (reduce r x)
                     | otherwise = helper (x+1) y r

--filtered_accumulate filter reduce [] result = result
--filtered_accumulate filter reduce (x:xs) result | filter x = filtered_accumulate filter reduce xs (reduce result x)
--                                                | otherwise = filtered_accumulate filter reduce xs result

--filtered_accumulate filter reduce range result = helper range result
--  where helper [] r = r
--        helper (x:xs) r | filter x = helper xs (reduce r x)
--                        | otherwise = helper xs r

--f1 a b = filtered_accumulate (\x -> rem (x-a) 13 == 12) (+) [a..b] 0
--f2 a b = filtered_accumulate odd (\r x -> (r+x*x)) [(a+1)..(b-1)] 0
--f3 a b = filtered_accumulate (\x -> (rem x 3) == 0) (\r x -> (product [1..x])*r ) [(a+1)..(b-1)] 1   
--f4 a b = filtered_accumulate prime (\r x -> (r+x*x)) [a..b] 0
--f5 a   = filtered_accumulate (\x -> (gcd a x) == 1) (*) [1..(a-1)] 1

f1 a b = filtered_accumulate (\x -> rem (x-a) 13 == 12) (+) a b 0
f2 a b = filtered_accumulate odd (\r x -> (r+x*x)) (a+1) (b-1) 0
f3 a b = filtered_accumulate (\x -> (rem x 3) == 0) (\r x -> (fact x)*r ) (a+1) (b-1) 1   
f4 a b = filtered_accumulate prime (\r x -> (r+x*x)) a b 0
f5 a   = filtered_accumulate (\x -> (gcd a x) == 1) (*) 1 (a-1) 1 

prime n = helper (floor (sqrt (fromIntegral n)))
  where helper x | x == 1 = n /= 1
                 | rem n x == 0 = False
                 | otherwise = (helper (x-1))

--prime n = helper [2..(floor (sqrt (fromIntegral n)))]
--  where helper [] = n /= 1
--        helper (x:xs) | rem n x == 0 = False
--                      | otherwise = (helper xs)


fact x | x == 1 = 1
       | otherwise = x * fact (x-1)

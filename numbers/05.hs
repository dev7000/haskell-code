carmichael n | n < 1 = error ""
             | otherwise = helper1 1 0 
  where helper1 x c | c == n = (x-1)
                    | iscarmichael x = helper1 (x+1) (c+1)   
                    | otherwise = helper1 (x+1) c
        iscarmichael x = helper2 1 x 0
        helper2 x n c | c == (n-1) = False
                      | x == n = True
                      | gcd x n /= 1 = helper2 (x+1) n c
                      | modexp x (n-1) n == 1 = helper2 (x+1) n (c+1)
                      | otherwise = False

modexp x y n = helper x y n 1 x
  where helper p q n a b | q == 0 = mod a n
                         | otherwise = helper p (quot q 2) n 
                               (mod (a*(1+(b-1)*(rem q 2))) n) (b*b)
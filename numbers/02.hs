ak_mult x y | x == 0 = 0
            | mod x 2 == 0 = ak_mult (quot x 2) (2*y) 
            | otherwise = ak_mult (quot x 2) (2*y) + y*rem x 2

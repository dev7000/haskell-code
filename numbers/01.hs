coeffs a b | b == 0 = (1*signum(a),0)
           | otherwise = (y,x-y*(div a b))
  where (x,y) = coeffs b (mod a b)

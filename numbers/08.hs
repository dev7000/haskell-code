wordset = ["zero","one","two","three","four","five","six","seven","eight","nine",
         "ten","eleven","tweleve","thirteen","fourteen","fifteen","sixteen",
         "seventeen","eighteen","nineteen","twenty","thirty","forty","fifty",
         "sixty","seventy","eighty","ninety"]

word x = helper wordset x   
  where helper (x:xs) n | n == 0 = x
                        | otherwise = helper xs (n-1) 

--convert n | n < 20 = wordset!!n 
--          | n < 100 = helper (wordset!!(quot n 10 + 18)) (rem n 10) False
--          | n < 1000 = helper ((convert (quot n 100)) ++ " hundred") (rem n 100) True
--          | otherwise = helper ((convert (quot n 1000)) ++ " thousand") (rem n 1000) True
--
--  where helper w n and | n == 0 = w
--                       | and = w ++ " and " ++ (convert n)
--                       | otherwise = w ++ " " ++ (convert n)


--wordset x | x==0 = "zero"
--          | x==1 = "one"
--          | x==2 = "two"
--          | x==3 = "three"
--          | x==4 = "four"
--          | x==5 = "five"
--          | x==6 = "six"
--          | x==7 = "seven"
--          | x==8 = "eight"
--          | x==9 = "nine"
--          | x==10 = "ten"
--          | x==11 = "eleven"
--          | x==12 = "tweleve"
--          | x==13 = "thirteen"
--          | x==14 = "fourteen"
--          | x==15 = "fifteen"
--          | x==16 = "sixteen"
--          | x==17 = "seventeen"
--          | x==18 = "eighteen"
--          | x==19 = "nineteen"
--          | x==20 = "twenty"
--          | x==21 = "thirty"
--          | x==22 = "forty"
--          | x==23 = "fifty"
--          | x==24 = "sixty"
--          | x==25 = "seventy"
--          | x==26 = "eighty"
--          | x==27 = "ninety"
           


--convert n | n < 20 = wordset n 
--          | n < 100 = helper (wordset (quot n 10 + 18)) (rem n 10) False
--          | n < 1000 = helper ((convert (quot n 100)) ++ " hundred") (rem n 100) True
--          | otherwise = helper ((convert (quot n 1000)) ++ " thousand") (rem n 1000) True
--
--  where helper w n and | n == 0 = w
--                       | and = w ++ " and " ++ (convert n)
--                       | otherwise = w ++ " " ++ (convert n)


convert n | n < 20 = word n 
          | n < 100 = helper (word (quot n 10 + 18)) (rem n 10) False
          | n < 1000 = helper ((convert (quot n 100)) ++ " hundred") (rem n 100) True
          | otherwise = helper ((convert (quot n 1000)) ++ " thousand") (rem n 1000) True

  where helper w n and | n == 0 = w
                       | and = w ++ " and " ++ (convert n)
                       | otherwise = w ++ " " ++ (convert n)





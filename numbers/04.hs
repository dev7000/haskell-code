modexp x y n = helper x y n 1 x
  where helper p q n a b | q == 0 = mod a n
                         | otherwise = helper p (quot q 2) n 
                               (mod (a*(1+(b-1)*(rem q 2))) n) (b*b)

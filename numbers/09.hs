-- data Peano = Zero | Succ Peano| Neg Peano deriving (Show)

-- intToPeano x | x == 0 = Zero
--              | x > 0 = Succ (intToPeano (x-1))
--              | otherwise = Neg (intToPeano (-x))

-- peanoToInt Zero = 0
-- peanoToInt (Succ x) = 1 + (peanoToInt x)
-- peanoToInt (Neg x) = (-(peanoToInt x))  

-- peanoeven Zero = True
-- peanoeven (Succ x) = peanoodd x
-- peanoeven (Neg x) = peanoeven x

-- peanoodd Zero = False
-- peanoodd (Succ x) = peanoeven x
-- peanoodd (Neg x) = peanoodd x

-- instance  Num Peano  where
--     (+) = operatorPeano (+)
--     (-) = operatorPeano (-)
--     (*) = operatorPeano (*)
--     negate      = intToPeano.negate.peanoToInt 
--     fromInteger = intToPeano
--     abs = intToPeano.abs.peanoToInt
--     signum = signum.peanoToInt

-- operatorPeano f x y = intToPeano (f intx inty)
--   where intx = peanoToInt x
--         inty = peanoToInt y



data Sign = Pos | Neg deriving (Show)
data Peano = Zero | Succ Peano  deriving (Show)
data Whole = Whole Sign Peano deriving (Show)

intToPeano x | x == 0 = Whole Pos Zero
             | x > 0 = let (Whole Pos z) = (intToPeano (x-1)) in (Whole Pos (Succ z))
             | otherwise = negatePeano (intToPeano (-x))

peanoToInt (Whole Pos Zero) = 0
peanoToInt (Whole Pos (Succ x)) = 1 + (peanoToInt (Whole Pos x))
peanoToInt (Whole Neg x) = (-(peanoToInt (Whole Pos x)))  


peanoeven (Whole Pos Zero) = True
peanoeven (Whole Pos (Succ x)) = peanoodd (Whole Pos x)
peanoeven (Whole Neg x) = peanoeven (Whole Pos x)

peanoodd (Whole Pos Zero) = False
peanoodd (Whole Pos (Succ x)) = peanoeven (Whole Pos x)
peanoodd (Whole Neg x) = peanoodd (Whole Pos x)


instance Num Whole where
      (+) = plusPeano
      (-) = minusPeano
      (*) = multPeano
      negate = negatePeano
      fromInteger = intToPeano
      abs = absPeano
      signum = signumPeano

plusPeano (Whole Pos Zero) y = y
plusPeano (Whole Pos (Succ x)) y = increment (plusPeano (Whole Pos x) y)
  where increment (Whole Neg Zero) = Whole Pos (Succ Zero)
        increment (Whole Neg (Succ x)) = Whole Neg x
        increment (Whole Pos x) = Whole Pos (Succ x)
plusPeano (Whole Neg x) y = negatePeano (plusPeano (Whole Pos x) (negatePeano y))

minusPeano x y = plusPeano x (negatePeano y)

multPeano (Whole Pos Zero) y = Whole Pos Zero
multPeano (Whole Pos x) (Whole Neg y) = negatePeano (multPeano (Whole Pos x) (Whole Neg y))
multPeano (Whole Neg x) (Whole Pos y) = negatePeano (multPeano (Whole Pos x) (Whole Neg y))
multPeano (Whole Neg x) (Whole Neg y) = multPeano (Whole Pos x) (Whole Neg y)
multPeano (Whole Pos (Succ x)) y = plusPeano y (multPeano (Whole Pos x) y)

negatePeano (Whole Neg x) = Whole Pos x
negatePeano (Whole Pos x) = Whole Neg x

absPeano (Whole Neg x) = Whole Pos x
absPeano x = x

signumPeano (Whole _ Zero) = 0
signumPeano (Whole Pos _) = 1
signumPeano (Whole Neg _) = -1
heftyTerms t = count
  where term n | n == 0 = (0,(1,1))
               | otherwise = let (c,(x,y))=term(n-1) in (c+(check x y),(2*y+x,y+x))
            
        (count,rest) = term (t+1) 
        check x y | y == 0 = signum x
                  | otherwise = check (quot x 10) (quot y 10) 

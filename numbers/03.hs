my_div x y | x == 0 = (0,0)
           | otherwise = (2*a+ quot r y,rem r y)
  where (a,b) = my_div (quot x 2) y
        r = 2*b+rem x 2

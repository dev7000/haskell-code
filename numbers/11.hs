import Data.Ratio

depth r = let (c,p) = depthposition (0%1) undefined 0 True 0 r in c
position r = let (c,p) = depthposition (0%1) undefined 0 True 0 r in p

depthposition l h c b p r | fromRational(m) < fromRational(r) = depthposition m h (c+1) b ((2*p)+1) r
                          | fromRational(m) > fromRational(r) = depthposition l m (c+1) False (2*p) r
                          | otherwise = (c,p) 
  where m = (numerator l + (u_numerator h)) % (denominator l + (u_denominator h))
        u_numerator x | b = 1      
                      | otherwise = numerator x
        u_denominator x | b = 0      
                        | otherwise = denominator x
